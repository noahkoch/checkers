require './board'
class Game
  attr_accessor :b, :current_player, :players
  def initialize
    @b = Board.new
    @players = {red: HumanPlayer.new(:red), black: HumanPlayer.new(:black)}
    @current_player = :red
    b.create_board
    play
  end
  def play
    loop do 
      b.render_board
      from_coord = prompt_first
      initial_check = players[current_player].check_first(from_coord, b)
      if initial_check == true || initial_check == :del
        to_coord = prompt_second
        if b.update_board(from_coord, to_coord)
          @current_player = (current_player == :red ? :black : :red)
          p "Your turn, #{current_player.to_s.capitalize}"
          if initial_check == :del
            just_fucking_delete_already(from_coord,to_coord)
          end
        end
      end
    end
  end
  
  def just_fucking_delete_already(from,to)
    p 'it got here, fuck'
    diff = [(from[0]-to[0]),(from[0]-to[0])]
    p diff.map!{|x| x/2}
    diff = [(from[0]-diff[0]),(diff[1]+from[1])]
    delete_this = b.pieces.select{|piece| piece.pos == diff}
    p delete_this[0]
    b.pieces.delete(delete_this[0])
  end
  
  def prompt_first
    puts "choose a piece to move"
    from_coord = gets.chomp.split(",").map{|x| x.to_i }
  end
    
  def prompt_second
    puts "move to"
    to_coord = gets.chomp.split(",").map{|x| x.to_i }
  end

end

class HumanPlayer
  attr_accessor :color
  def initialize(color)
    @color = color
  end
  
  def check_first(coord, board)
    require_jump = board.pieces.select{|piece| piece.color == color && piece.jump_required}
    unless require_jump.empty?
      unless require_jump[0].pos == coord
        print "You have a required jump at #{require_jump[0].pos}! \n" 
        return false
      end
      return :del
    end 
    if board.occupant(coord).nil?
      false
    else
      board.occupant(coord).color == color
    end
  end
end
Game.new
