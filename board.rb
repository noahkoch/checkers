class Board
  attr_accessor :board, :pieces
  def initialize
    @board = Array.new(10) {Array.new(10)}
    @pieces = []
  end
  
  def create_board
    create_pieces
  end
  
  def create_pieces
    [:red,:black].each do |color|
      20.times do |x|
        pos = create_piece_position(color, x)
        pieces << Piece.new(color, pos, self)
      end
    end
  end
    
  def create_piece_position(color, increment)
    # Defines what row the checker will be placed in
    i = (increment / 5)
    # Defines what place in the row the checker will be
    j = i % 2 == 0 ? (increment - ( 5 * i )) * 2 : ((increment - ( 5 * i )) * 2 ) + 1
    i += (color == :red ? 0 : 6)
    # Returns the position array
    [i,j]
  end
  
  def render_board
    pieces.each_with_index do |piece, x|
      board[piece.pos[0]][piece.pos[1]] = (piece.color == :red ? " R " : " B ")
    end
    board.each do |column|
      row = []
      column.each do |piece|
        row << (piece.nil? ? "___" : piece)
      end
      p row.join(" ")
    end
  end
  
  def update_board(from_coord, to_coord)
    from_x, from_y = from_coord
    to_x, to_y = to_coord
    
    moving_piece = pieces.select {|piece| piece.pos == from_coord}
    moving_piece = moving_piece.first
    p moving_piece.valid_moves
    if moving_piece.valid_moves.include?(to_coord) 
      board[from_x][from_y] = nil
      board[to_x][to_y] = moving_piece.color
      moving_piece.pos = to_coord
      return true
    end
    false
  end
 
  def occupant(coord)
    x, y = coord
    selected = pieces.select{|piece| piece.pos == [x, y]}
    selected[0]
  end
  
end

class Piece
  attr_accessor :pos, :color, :level, :board, :direction, :moves, :require_jump
  def initialize(color, pos, board)
    @color = color
    @pos = pos
    @board = board
    @level = 0 # King or single player
    @direction = (color == :red ? 1 : -1) # For determining the appropriate direction of the color you're using
    @moves = []
    @require_jump = false
  end
  
  def valid_moves
    @moves = []
    @require_jump = false
    transformations = [[(1 * direction),-1], [(1 * direction),1]]
    transformations.each do |move|
      moves << [(move[0] + pos[0]), (move[1] + pos[1])]
    
      intersection = (find_enemies & moves)
      if (intersection).count > 0
        @require_jump = true
        unless allow_jumps(intersection).empty?
          delete_piece = board.pieces.select{|piece| piece.pos == intersection[0]}
          delete_this_shit(delete_piece)
          moves = allow_jumps(intersection) 
          return moves
        end
      end
    end
    return moves
  end
  
  def delete_this_shit(fuck)
    board.pieces.delete(fuck)
  end
  
  def find_enemies
    enemy_moves = []
    board.pieces.each do |piece|
      if piece.color != color
        enemy_moves << piece.pos
      end
    end
    enemy_moves
  end
  
  def allow_jumps(intersection)
    moves = []
    intersection.each do |coord|

      if board.occupant([(direction + coord[0]),(direction + coord[1])]).nil?
        moves << [(direction + coord[0]),(direction + coord[1])]
        
      elsif board.occupant([(direction + coord[0]),((direction - coord[1]))]).nil?

         moves << [(direction + coord[0]),((direction - coord[1]))]
      end
    end
    moves
  end
  
  def jump_required
    valid_moves
    return pos if @require_jump == true
    nil
  end
end